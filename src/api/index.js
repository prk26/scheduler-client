export const BASE_URL = "https://localhost:44392/api";

export const COORDINATORS_URL = BASE_URL + '/coordinators';
export const JOBS_URL = BASE_URL + '/jobs';
export const JOBTASKS_URL = BASE_URL + '/jobtasks';
export const LEAVE_URL = BASE_URL + '/leave';
export const OUTOFSERVICE_URL = BASE_URL + '/outofservice';
export const RESOURCES_URL = BASE_URL + '/resources';
export const TRAINING_URL = BASE_URL + '/training';
export const WORKERS_URL = BASE_URL + '/workers';