import React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "../components/common/header";
import Home from "../components/home";
import Reports from "../components/reports";
import About from "../components/about";
import PageNotFound from "../components/pageNotFound";
import {
  CoordinatorsList,
  CoordinatorDetail,
  CreateCoordinatorForm,
  EditCoordinatorForm
} from "../components/coordinators";
import {
  CreateJobForm,
  EditJobForm,
  JobDetail,
  JobsList
} from "../components/jobs";
import {
  JobTaskDetail,
  CreateJobTaskForm,
  EditJobTaskForm
} from "../components/jobTasks";
import {
  CreateResourceForm,
  EditResourceForm,
  ResourcesList,
  ResourceDetail,
  ResourcesSchedule
} from "../components/resources";
import {
  CreateOutOfServiceForm,
  EditOutOfServiceForm
} from "../components/outOfServices";
import {
  CreateWorkerForm,
  EditWorkerForm,
  WorkerDetail,
  WorkersList,
  WorkersSchedule
} from "../components/workers";
import { CreateLeaveForm, EditLeaveForm } from "../components/leave";
import {
  CreateTrainingForm,
  EditTrainingForm,
  TrainingList,
  TrainingDetail
} from "../components/training";
import Footer from "../components/common/footer";
import Routes from "../routes";

function App() {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path={Routes.home} component={Home} />
        <Route path={Routes.reports} component={Reports} />
        <Route path={Routes.about} component={About} />

        <Route
          exact
          path={Routes.coordinators.LIST}
          component={CoordinatorsList}
        />
        <Route
          path={Routes.coordinators.CREATE}
          component={CreateCoordinatorForm}
        />
        <Route
          path={Routes.coordinators.EDIT}
          component={EditCoordinatorForm}
        />
        <Route
          path={Routes.coordinators.DETAIL}
          component={CoordinatorDetail}
        />

        <Route exact path={Routes.jobs.LIST} component={JobsList} />
        <Route path={Routes.jobs.CREATE} component={CreateJobForm} />
        <Route path={Routes.jobs.EDIT} component={EditJobForm} />
        <Route path={Routes.jobs.DETAIL} component={JobDetail} />

        <Route path={Routes.jobTasks.CREATE} component={CreateJobTaskForm} />
        <Route path={Routes.jobTasks.EDIT} component={EditJobTaskForm} />
        <Route path={Routes.jobTasks.DETAIL} component={JobTaskDetail} />

        <Route exact path={Routes.resources.LIST} component={ResourcesList} />
        <Route path={Routes.resources.CREATE} component={CreateResourceForm} />
        <Route path={Routes.resources.EDIT} component={EditResourceForm} />
        <Route path={Routes.resources.DETAIL} component={ResourceDetail} />

        <Route
          path={Routes.outOfServices.CREATE}
          component={CreateOutOfServiceForm}
        />
        <Route
          path={Routes.outOfServices.EDIT}
          component={EditOutOfServiceForm}
        />

        <Route exact path={Routes.workers.LIST} component={WorkersList} />
        <Route path={Routes.workers.CREATE} component={CreateWorkerForm} />
        <Route path={Routes.workers.EDIT} component={EditWorkerForm} />
        <Route path={Routes.workers.DETAIL} component={WorkerDetail} />

        <Route path={Routes.leave.CREATE} component={CreateLeaveForm} />
        <Route path={Routes.leave.EDIT} component={EditLeaveForm} />

        <Route exact path={Routes.training.LIST} component={TrainingList} />
        <Route path={Routes.training.CREATE} component={CreateTrainingForm} />
        <Route path={Routes.training.EDIT} component={EditTrainingForm} />
        <Route path={Routes.training.DETAIL} component={TrainingDetail} />

        <Route path={Routes.schedules.workers} component={WorkersSchedule} />
        <Route
          path={Routes.schedules.resources}
          component={ResourcesSchedule}
        />

        <Route component={PageNotFound} />
      </Switch>
      <Footer />
    </>
  );
}

export default App;
