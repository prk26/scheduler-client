import CreateTrainingForm from './createTrainingForm';
import EditTrainingForm from './editTrainingForm';
import TrainingList from './trainingList';
import TrainingDetail from './trainingDetail';

export { CreateTrainingForm, EditTrainingForm, TrainingList, TrainingDetail };