import styled from 'styled-components';

const Title = styled.div`
    display: flex;

    h2 {
        margin-right: 15px;
    }
`;

export default Title;