import styled from 'styled-components';
import Breadcrumb from 'react-bootstrap/Breadcrumb';

const StyledBreadcrumb = styled(Breadcrumb)`
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075);

    ol {
        background-color: ${props => props.theme.colours.breadcrumb};
        margin-bottom: 0;
    }
`;

export default StyledBreadcrumb;