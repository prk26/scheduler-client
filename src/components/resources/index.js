import CreateResourceForm from './createResourceForm';
import EditResourceForm from './editResourceForm';
import ResourcesList from './resourcesList';
import ResourceDetail from './resourceDetail';
import ResourcesSchedule from './resourcesSchedule';

export { CreateResourceForm, EditResourceForm, ResourcesList, ResourceDetail, ResourcesSchedule };